#! /bin/bash



# Load the configuration

if
	[ ! -f configuration.sh ] \
	&& [ ! -f configuration.example.sh ]
then
	echo Error: Configuration files not found.
	exit 1
fi

if [ ! -f configuration.sh ]
then
	cp configuration.example.sh configuration.sh
fi

source configuration.sh @



# Clean the public dir

rm -rf "$PROJECT_DIR/public"

mkdir "$PROJECT_DIR/public"



# HTML

cat "$PROJECT_DIR/private/app/fw/base/html/head.html" > "$PROJECT_DIR/public/index.html"

for file in `find "$PROJECT_DIR/private/app" -name *.html`
do
	if
		[ $file != "$PROJECT_DIR/private/app/fw/base/html/head.html" ] \
		&& [ $file != "$PROJECT_DIR/private/app/fw/base/html/tail.html" ]
	then
		cat "$file" >> "$PROJECT_DIR/public/index.html"
	fi
done

cat "$PROJECT_DIR/private/app/fw/base/html/tail.html" >> "$PROJECT_DIR/public/index.html"



# CSS

mkdir "$PROJECT_DIR/public/css"

cat "$PROJECT_DIR/private/app/fw/base/css/head.css" > "$PROJECT_DIR/public/css/style.css"

for file in `find "$PROJECT_DIR/private/app" -name *.css`
do
	if
		[ $file != "$PROJECT_DIR/private/app/fw/base/css/head.css" ]
	then
		cat "$file" >> "$PROJECT_DIR/public/css/style.css"
	fi
done



# JS

mkdir "$PROJECT_DIR/public/js"

cat "$PROJECT_DIR/private/app/fw/base/js/head.js" > "$PROJECT_DIR/public/js/script.js"

for file in `find "$PROJECT_DIR/private/app" -name *.js`
do
	if
		[ $file != "$PROJECT_DIR/private/app/fw/base/js/head.js" ] \
		&& [ $file != "$PROJECT_DIR/private/app/fw/base/js/tail.js" ]
	then
		cat "$file" >> "$PROJECT_DIR/public/js/script.js"
	fi
done

cat "$PROJECT_DIR/private/app/fw/base/js/tail.js" >> "$PROJECT_DIR/public/js/script.js"



# ASSET

mkdir "$PROJECT_DIR/public/asset"

cp "$PROJECT_DIR/private/app/demo/base/asset/"* "$PROJECT_DIR/public/asset"
