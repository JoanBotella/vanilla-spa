
const Demo_App = function (
	demoRouter
)
{

	this.run = function ()
	{
		const baseUrl = 'http://localhost/vanilla-spa/repo';

		let routeId = window.location.toString().substring(
			baseUrl.length
		);
		if (routeId[0] == '/')
		{
			routeId = routeId.substring(1);
		}

		console.debug('[Demo_App] routeId: ' + routeId);

		const bodyElement = document.querySelector('body');
		bodyElement.setAttribute('data-route_id', routeId);

		const data = {
			routeId: routeId
		};

		window.history.replaceState(data, null, '');

		window.onpopstate = function (event)
		{
			const routeId = history.state.routeId;

			console.debug('[Demo_App.onpopstate] routeId: ' + routeId);

			const bodyElement = document.querySelector('body');
			bodyElement.setAttribute('data-route_id', routeId);

			demoRouter.route();
		}

		demoRouter.route();
	}

}
