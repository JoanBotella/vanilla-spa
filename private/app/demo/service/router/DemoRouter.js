
const Demo_Router = function (serviceContainer)
{

	this.route = function () 
	{
		const routeId = this._getRouteId();
		let controller;

		console.debug('[Demo_Router.route] routeId: ' + routeId);

		switch (routeId)
		{
			case Demo_Book_Front_Page_Home_Constant.ROUTE_ID:
				controller = serviceContainer.getDemoBookFrontPageHomeController();
				break;

			case Demo_Book_Front_Page_About_Constant.ROUTE_ID:
				controller = serviceContainer.getDemoBookFrontPageAboutController();
				break;

			case Demo_Book_Front_Page_Contact_Constant.ROUTE_ID:
				controller = serviceContainer.getDemoBookFrontPageContactController();
				break;

			default:
				controller = serviceContainer.getDemoBookFrontPageNotFoundController();
				break;
		}

		controller.run();
	}

		this._getRouteId = function ()
		{
			const bodyElement = document.querySelector('body');

			let routeId = bodyElement.getAttribute('data-route_id');
			if (
				routeId === undefined
				|| routeId === null
				|| routeId === ''
			)
			{
				routeId = Demo_Book_Front_Page_Home_Constant.ROUTE_ID;
			}

			return routeId;
		}

};
