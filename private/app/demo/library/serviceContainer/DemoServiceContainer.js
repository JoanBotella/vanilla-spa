
const Demo_ServiceContainer = function ()
{
	this._fwServiceContainer = new Fw_ServiceContainer();

	// Demo

	this.getDemoApp = function ()
	{
		return new Demo_App(
			this.getDemoRouter()
		);
	}

	this.getDemoRouter = function ()
	{
		return new Demo_Router(
			this
		);
	}

	// Demo_Book_Front

	this.getDemoBookFrontWidgetLayout = function ()
	{
		return new Demo_Book_Front_Widget_Layout(
			this._fwServiceContainer.getFwTemplateImporter(),
			this.getDemoBookFrontWidgetNav(),
			this.getDemoBookFrontWidgetFooter(),
			this.getDemoRouter()
		);
	}

	this.getDemoBookFrontWidgetNav = function ()
	{
		return new Demo_Book_Front_Widget_Nav(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	this.getDemoBookFrontWidgetFooter = function ()
	{
		return new Demo_Book_Front_Widget_Footer(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	// Demo_Book_Front_Page_Home

	this.getDemoBookFrontPageHomeController = function ()
	{
		return new Demo_Book_Front_Page_Home_Controller(
			this.getDemoBookFrontWidgetLayout(),
			this.getDemoBookFrontPageHomeWidgetMain()
		);
	}

	this.getDemoBookFrontPageHomeWidgetMain = function ()
	{
		return new Demo_Book_Front_Page_Home_Widget_Main(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	// Demo_Book_Front_Page_Contact

	this.getDemoBookFrontPageContactController = function ()
	{
		return new Demo_Book_Front_Page_Contact_Controller(
			this.getDemoBookFrontWidgetLayout(),
			this.getDemoBookFrontPageContactWidgetMain()
		);
	}

	this.getDemoBookFrontPageContactWidgetMain = function ()
	{
		return new Demo_Book_Front_Page_Contact_Widget_Main(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	// Demo_Book_Front_Page_About

	this.getDemoBookFrontPageAboutController = function ()
	{
		return new Demo_Book_Front_Page_About_Controller(
			this.getDemoBookFrontWidgetLayout(),
			this.getDemoBookFrontPageAboutWidgetMain()
		);
	}

	this.getDemoBookFrontPageAboutWidgetMain = function ()
	{
		return new Demo_Book_Front_Page_About_Widget_Main(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	// Demo_Book_Front_Page_NotFound

	this.getDemoBookFrontPageNotFoundController = function ()
	{
		return new Demo_Book_Front_Page_NotFound_Controller(
			this.getDemoBookFrontWidgetLayout(),
			this.getDemoBookFrontPageNotFoundWidgetMain()
		);
	}

	this.getDemoBookFrontPageNotFoundWidgetMain = function ()
	{
		return new Demo_Book_Front_Page_NotFound_Widget_Main(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}


}
