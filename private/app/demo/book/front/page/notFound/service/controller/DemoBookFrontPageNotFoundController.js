
const Demo_Book_Front_Page_NotFound_Controller = function (
	demoBookFrontWidgetLayout,
	demoBookFrontPageNotFoundWidgetMain
)
{

	this.run = function ()
	{
		console.debug('[Demo_Book_Front_Page_NotFound_Controller.run]');

		this._render();
	}

		this._render = function ()
		{
			document.title = 'Page Not Found | Vanilla SPA';

			let mainWidgetElement = demoBookFrontPageNotFoundWidgetMain.render();

			let layoutWidgetElement = demoBookFrontWidgetLayout.render(
				mainWidgetElement
			);

			let layoutBoxElement = document.querySelector('[data-box="layout"]');
			layoutBoxElement.innerHTML = '';
			layoutBoxElement.appendChild(layoutWidgetElement);
		}

}