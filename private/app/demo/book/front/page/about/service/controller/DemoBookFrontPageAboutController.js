
const Demo_Book_Front_Page_About_Controller = function (
	demoBookFrontWidgetLayout,
	demoBookFrontPageAboutWidgetMain
)
{

	this.run = function ()
	{
		console.debug('[Demo_Book_Front_Page_About_Controller.run]');

		this._render();
	}

		this._render = function ()
		{
			document.title = 'About | Vanilla SPA';

			let mainWidgetElement = demoBookFrontPageAboutWidgetMain.render();

			let layoutWidgetElement = demoBookFrontWidgetLayout.render(
				mainWidgetElement
			);

			let layoutBoxElement = document.querySelector('[data-box="layout"]');
			layoutBoxElement.innerHTML = '';
			layoutBoxElement.appendChild(layoutWidgetElement);
		}

}