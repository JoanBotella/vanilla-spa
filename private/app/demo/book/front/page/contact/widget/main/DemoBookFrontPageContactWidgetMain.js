
const Demo_Book_Front_Page_Contact_Widget_Main = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Page_Contact_Widget_Main.render]');

		let widgetElement = fwTemplateImporter.import(
			'demo-book-front-page-contact-widget-main'
		);

		return widgetElement;
	}

}
