
const Demo_Book_Front_Page_Contact_Controller = function (
	demoBookFrontWidgetLayout,
	demoBookFrontPageContactWidgetMain
)
{

	this.run = function ()
	{
		console.debug('[Demo_Book_Front_Page_Contact_Controller.run]');

		this._render();
	}

		this._render = function ()
		{
			document.title = 'Contact | Vanilla SPA';

			let mainWidgetElement = demoBookFrontPageContactWidgetMain.render();

			let layoutWidgetElement = demoBookFrontWidgetLayout.render(
				mainWidgetElement
			);

			let layoutBoxElement = document.querySelector('[data-box="layout"]');
			layoutBoxElement.innerHTML = '';
			layoutBoxElement.appendChild(layoutWidgetElement);
		}

}