
const Demo_Book_Front_Page_Home_Controller = function (
	demoBookFrontWidgetLayout,
	demoBookFrontPageHomeWidgetMain
)
{

	this.run = function ()
	{
		console.debug('[Demo_Book_Front_Page_Home_Controller.run]');

		this._render();
	}

		this._render = function ()
		{
			document.title = 'Home | Vanilla SPA';

			let mainWidgetElement = demoBookFrontPageHomeWidgetMain.render();

			let layoutWidgetElement = demoBookFrontWidgetLayout.render(
				mainWidgetElement
			);

			let layoutBoxElement = document.querySelector('[data-box="layout"]');
			layoutBoxElement.innerHTML = '';
			layoutBoxElement.appendChild(layoutWidgetElement);
		}

}