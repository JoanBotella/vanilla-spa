
const Demo_Book_Front_Page_Home_Widget_Main = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Page_Home_Widget_Main.render]');

		let widgetElement = fwTemplateImporter.import(
			'demo-book-front-page-home-widget-main'
		);

		return widgetElement;
	}

}
