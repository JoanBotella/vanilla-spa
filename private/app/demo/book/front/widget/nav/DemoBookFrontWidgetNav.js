
const Demo_Book_Front_Widget_Nav = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Widget_Nav.render]');

		let widgetElement = fwTemplateImporter.import(
			'demo-book-front-widget-nav'
		);

		return widgetElement;
	}

}
