
const Demo_Book_Front_Widget_Footer = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Widget_Footer.render]');

		let widgetElement = fwTemplateImporter.import(
			'demo-book-front-widget-footer'
		);

		return widgetElement;
	}

}
