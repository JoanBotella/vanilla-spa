
const Demo_Book_Front_Widget_Layout = function (
	fwTemplateImporter,
	demoBookFrontWidgetNav,
	demoBookFrontWidgetFooter,
	demoRouter
)
{

	this.render = function (
		mainWidgetElement
	)
	{
		console.debug('[Demo_Book_Front_Widget_Layout.render]');

		let layoutWidgetElement = fwTemplateImporter.import(
			'demo-book-front-widget-layout'
		);

		let navWidgetElement = demoBookFrontWidgetNav.render();
		let navBoxElement = layoutWidgetElement.querySelector('[data-box="nav"]');
		navBoxElement.appendChild(navWidgetElement);

		let footerWidgetElement = demoBookFrontWidgetFooter.render();
		let footerBoxElement = layoutWidgetElement.querySelector('[data-box="footer"]');
		footerBoxElement.appendChild(footerWidgetElement);

		let mainBoxElement = layoutWidgetElement.querySelector('[data-box="main"]');
		mainBoxElement.appendChild(mainWidgetElement);

		const linkElements = layoutWidgetElement.querySelectorAll('[data-link]');
		for (let index = 0; index < linkElements.length; index++)
		{
			linkElements[index].addEventListener(
				'click',
				function (event)
				{
					event.preventDefault();

					const routeId = event.target.getAttribute('data-link');

					const bodyElement = document.querySelector('body');
					bodyElement.setAttribute('data-route_id', routeId);

					const data = {
						routeId: routeId
					};

					window.history.pushState(data, '', data.routeId);

					demoRouter.route();
				}
			);
		}

		return layoutWidgetElement;
	}

}
