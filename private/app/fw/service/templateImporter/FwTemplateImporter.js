
const Fw_TemplateImporter = function ()
{

	this.import = function (
		templateId
	)
	{
		let template = document.querySelector('[data-template="' + templateId + '"]');
		return document.importNode(template.content, true);
	}

}