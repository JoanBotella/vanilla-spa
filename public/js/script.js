'use strict';

window.addEventListener(
	'DOMContentLoaded',
	function ()
	{

const Demo_Book_Front_Page_Contact_Controller = function (
	demoBookFrontWidgetLayout,
	demoBookFrontPageContactWidgetMain
)
{

	this.run = function ()
	{
		console.debug('[Demo_Book_Front_Page_Contact_Controller.run]');

		this._render();
	}

		this._render = function ()
		{
			document.title = 'Contact | Vanilla SPA';

			let mainWidgetElement = demoBookFrontPageContactWidgetMain.render();

			let layoutWidgetElement = demoBookFrontWidgetLayout.render(
				mainWidgetElement
			);

			let layoutBoxElement = document.querySelector('[data-box="layout"]');
			layoutBoxElement.innerHTML = '';
			layoutBoxElement.appendChild(layoutWidgetElement);
		}

}
const Demo_Book_Front_Page_Contact_Constant = {

	ROUTE_ID: 'contact'

}
const Demo_Book_Front_Page_Contact_Widget_Main = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Page_Contact_Widget_Main.render]');

		let widgetElement = fwTemplateImporter.import(
			'demo-book-front-page-contact-widget-main'
		);

		return widgetElement;
	}

}

const Demo_Book_Front_Page_Home_Controller = function (
	demoBookFrontWidgetLayout,
	demoBookFrontPageHomeWidgetMain
)
{

	this.run = function ()
	{
		console.debug('[Demo_Book_Front_Page_Home_Controller.run]');

		this._render();
	}

		this._render = function ()
		{
			document.title = 'Home | Vanilla SPA';

			let mainWidgetElement = demoBookFrontPageHomeWidgetMain.render();

			let layoutWidgetElement = demoBookFrontWidgetLayout.render(
				mainWidgetElement
			);

			let layoutBoxElement = document.querySelector('[data-box="layout"]');
			layoutBoxElement.innerHTML = '';
			layoutBoxElement.appendChild(layoutWidgetElement);
		}

}
const Demo_Book_Front_Page_Home_Constant = {

	ROUTE_ID: 'home'

}
const Demo_Book_Front_Page_Home_Widget_Main = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Page_Home_Widget_Main.render]');

		let widgetElement = fwTemplateImporter.import(
			'demo-book-front-page-home-widget-main'
		);

		return widgetElement;
	}

}

const Demo_Book_Front_Page_NotFound_Controller = function (
	demoBookFrontWidgetLayout,
	demoBookFrontPageNotFoundWidgetMain
)
{

	this.run = function ()
	{
		console.debug('[Demo_Book_Front_Page_NotFound_Controller.run]');

		this._render();
	}

		this._render = function ()
		{
			document.title = 'Page Not Found | Vanilla SPA';

			let mainWidgetElement = demoBookFrontPageNotFoundWidgetMain.render();

			let layoutWidgetElement = demoBookFrontWidgetLayout.render(
				mainWidgetElement
			);

			let layoutBoxElement = document.querySelector('[data-box="layout"]');
			layoutBoxElement.innerHTML = '';
			layoutBoxElement.appendChild(layoutWidgetElement);
		}

}
const Demo_Book_Front_Page_NotFound_Constant = {

	ROUTE_ID: 'not_found'

}
const Demo_Book_Front_Page_NotFound_Widget_Main = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Page_NotFound_Widget_Main.render]');

		let widgetElement = fwTemplateImporter.import(
			'demo-book-front-page-not_found-widget-main'
		);

		return widgetElement;
	}

}

const Demo_Book_Front_Page_About_Controller = function (
	demoBookFrontWidgetLayout,
	demoBookFrontPageAboutWidgetMain
)
{

	this.run = function ()
	{
		console.debug('[Demo_Book_Front_Page_About_Controller.run]');

		this._render();
	}

		this._render = function ()
		{
			document.title = 'About | Vanilla SPA';

			let mainWidgetElement = demoBookFrontPageAboutWidgetMain.render();

			let layoutWidgetElement = demoBookFrontWidgetLayout.render(
				mainWidgetElement
			);

			let layoutBoxElement = document.querySelector('[data-box="layout"]');
			layoutBoxElement.innerHTML = '';
			layoutBoxElement.appendChild(layoutWidgetElement);
		}

}
const Demo_Book_Front_Page_About_Constant = {

	ROUTE_ID: 'about'

}
const Demo_Book_Front_Page_About_Widget_Main = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Page_About_Widget_Main.render]');

		const widgetElement = fwTemplateImporter.import(
			'demo-book-front-page-about-widget-main'
		);

		return widgetElement;
	}

}

const Demo_Book_Front_Widget_Nav = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Widget_Nav.render]');

		let widgetElement = fwTemplateImporter.import(
			'demo-book-front-widget-nav'
		);

		return widgetElement;
	}

}

const Demo_Book_Front_Widget_Layout = function (
	fwTemplateImporter,
	demoBookFrontWidgetNav,
	demoBookFrontWidgetFooter,
	demoRouter
)
{

	this.render = function (
		mainWidgetElement
	)
	{
		console.debug('[Demo_Book_Front_Widget_Layout.render]');

		let layoutWidgetElement = fwTemplateImporter.import(
			'demo-book-front-widget-layout'
		);

		let navWidgetElement = demoBookFrontWidgetNav.render();
		let navBoxElement = layoutWidgetElement.querySelector('[data-box="nav"]');
		navBoxElement.appendChild(navWidgetElement);

		let footerWidgetElement = demoBookFrontWidgetFooter.render();
		let footerBoxElement = layoutWidgetElement.querySelector('[data-box="footer"]');
		footerBoxElement.appendChild(footerWidgetElement);

		let mainBoxElement = layoutWidgetElement.querySelector('[data-box="main"]');
		mainBoxElement.appendChild(mainWidgetElement);

		const linkElements = layoutWidgetElement.querySelectorAll('[data-link]');
		for (let index = 0; index < linkElements.length; index++)
		{
			linkElements[index].addEventListener(
				'click',
				function (event)
				{
					event.preventDefault();

					const routeId = event.target.getAttribute('data-link');

					const bodyElement = document.querySelector('body');
					bodyElement.setAttribute('data-route_id', routeId);

					const data = {
						routeId: routeId
					};

					window.history.pushState(data, '', data.routeId);

					demoRouter.route();
				}
			);
		}

		return layoutWidgetElement;
	}

}

const Demo_Book_Front_Widget_Footer = function (
	fwTemplateImporter
)
{

	this.render = function ()
	{
		console.debug('[Demo_Book_Front_Widget_Footer.render]');

		let widgetElement = fwTemplateImporter.import(
			'demo-book-front-widget-footer'
		);

		return widgetElement;
	}

}

const Demo_Router = function (serviceContainer)
{

	this.route = function () 
	{
		const routeId = this._getRouteId();
		let controller;

		console.debug('[Demo_Router.route] routeId: ' + routeId);

		switch (routeId)
		{
			case Demo_Book_Front_Page_Home_Constant.ROUTE_ID:
				controller = serviceContainer.getDemoBookFrontPageHomeController();
				break;

			case Demo_Book_Front_Page_About_Constant.ROUTE_ID:
				controller = serviceContainer.getDemoBookFrontPageAboutController();
				break;

			case Demo_Book_Front_Page_Contact_Constant.ROUTE_ID:
				controller = serviceContainer.getDemoBookFrontPageContactController();
				break;

			default:
				controller = serviceContainer.getDemoBookFrontPageNotFoundController();
				break;
		}

		controller.run();
	}

		this._getRouteId = function ()
		{
			const bodyElement = document.querySelector('body');

			let routeId = bodyElement.getAttribute('data-route_id');
			if (
				routeId === undefined
				|| routeId === null
				|| routeId === ''
			)
			{
				routeId = Demo_Book_Front_Page_Home_Constant.ROUTE_ID;
			}

			return routeId;
		}

};

const Demo_App = function (
	demoRouter
)
{

	this.run = function ()
	{
		const baseUrl = 'http://localhost/vanilla-spa/repo';

		let routeId = window.location.toString().substring(
			baseUrl.length
		);
		if (routeId[0] == '/')
		{
			routeId = routeId.substring(1);
		}

		console.debug('[Demo_App] routeId: ' + routeId);

		const bodyElement = document.querySelector('body');
		bodyElement.setAttribute('data-route_id', routeId);

		const data = {
			routeId: routeId
		};

		window.history.replaceState(data, null, '');

		window.onpopstate = function (event)
		{
			// const routeId =
			// 	history.state === null
			// 		? ''
			// 		: history.state.routeId
			// ;

			const routeId = history.state.routeId;

			console.debug('[Demo_App.onpopstate] routeId: ' + routeId);

			const bodyElement = document.querySelector('body');
			bodyElement.setAttribute('data-route_id', routeId);

			demoRouter.route();
		}

		demoRouter.route();
	}

}

const Demo_ServiceContainer = function ()
{
	this._fwServiceContainer = new Fw_ServiceContainer();

	// Demo

	this.getDemoApp = function ()
	{
		return new Demo_App(
			this.getDemoRouter()
		);
	}

	this.getDemoRouter = function ()
	{
		return new Demo_Router(
			this
		);
	}

	// Demo_Book_Front

	this.getDemoBookFrontWidgetLayout = function ()
	{
		return new Demo_Book_Front_Widget_Layout(
			this._fwServiceContainer.getFwTemplateImporter(),
			this.getDemoBookFrontWidgetNav(),
			this.getDemoBookFrontWidgetFooter(),
			this.getDemoRouter()
		);
	}

	this.getDemoBookFrontWidgetNav = function ()
	{
		return new Demo_Book_Front_Widget_Nav(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	this.getDemoBookFrontWidgetFooter = function ()
	{
		return new Demo_Book_Front_Widget_Footer(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	// Demo_Book_Front_Page_Home

	this.getDemoBookFrontPageHomeController = function ()
	{
		return new Demo_Book_Front_Page_Home_Controller(
			this.getDemoBookFrontWidgetLayout(),
			this.getDemoBookFrontPageHomeWidgetMain()
		);
	}

	this.getDemoBookFrontPageHomeWidgetMain = function ()
	{
		return new Demo_Book_Front_Page_Home_Widget_Main(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	// Demo_Book_Front_Page_Contact

	this.getDemoBookFrontPageContactController = function ()
	{
		return new Demo_Book_Front_Page_Contact_Controller(
			this.getDemoBookFrontWidgetLayout(),
			this.getDemoBookFrontPageContactWidgetMain()
		);
	}

	this.getDemoBookFrontPageContactWidgetMain = function ()
	{
		return new Demo_Book_Front_Page_Contact_Widget_Main(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	// Demo_Book_Front_Page_About

	this.getDemoBookFrontPageAboutController = function ()
	{
		return new Demo_Book_Front_Page_About_Controller(
			this.getDemoBookFrontWidgetLayout(),
			this.getDemoBookFrontPageAboutWidgetMain()
		);
	}

	this.getDemoBookFrontPageAboutWidgetMain = function ()
	{
		return new Demo_Book_Front_Page_About_Widget_Main(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}

	// Demo_Book_Front_Page_NotFound

	this.getDemoBookFrontPageNotFoundController = function ()
	{
		return new Demo_Book_Front_Page_NotFound_Controller(
			this.getDemoBookFrontWidgetLayout(),
			this.getDemoBookFrontPageNotFoundWidgetMain()
		);
	}

	this.getDemoBookFrontPageNotFoundWidgetMain = function ()
	{
		return new Demo_Book_Front_Page_NotFound_Widget_Main(
			this._fwServiceContainer.getFwTemplateImporter()
		);
	}


}

const run = function ()
{
	const serviceContainer = new Demo_ServiceContainer();
	const app = serviceContainer.getDemoApp();
	app.run();
}

const Fw_TemplateImporter = function ()
{

	this.import = function (
		templateId
	)
	{
		let template = document.querySelector('[data-template="' + templateId + '"]');
		return document.importNode(template.content, true);
	}

}
const Fw_ServiceContainer = function ()
{

	this.getFwTemplateImporter = function ()
	{
		return new Fw_TemplateImporter();
	}

}
		run();

	}
);